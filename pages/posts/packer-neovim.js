import Head from 'next/head'
import Layout from '../../components/layout'

export default function packerNeovim() {
  return (
    <Layout>
      <Head>
        <title>Packer for Neovim</title>
      </Head>
      <h1>Packer for Neovim</h1>
      <p>Packer is a package manager for Neovim.</p>
    </Layout>
  )
}
