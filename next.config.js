// https://stackoverflow.com/questions/45141957/how-to-deploy-next-js-app-to-gitlab-github-pages  
// https://nextjs.org/docs/api-reference/next.config.js/cdn-support-with-asset-prefix  
module.exports = {
  assetPrefix: process.env.NODE_ENV === 'production' ? '/nextjs-sakato' : '',
}
